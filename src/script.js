

class User{

    constructor(){
        this.name="";
        this.courses=new Array();

    }

    addName(name){
        this.name=name;
    }

    addCourses(course) {
        // console.log('Moo, my name is ' + this.name);
        this.courses.push(course);
        
    }
}

function getInputValue() {
    // Selecting the input element and get its value 
    var inputVal = document.getElementById("myInput").value;
    // create object of user type
    var user = new User();

    user.addName(inputVal);
    //adding courses in user
    user.addCourses("Data Science");
    user.addCourses("Digital Marketing");
    user.addCourses("Computer Science");
    //adding name and courses in table
    addRow(user.name, user.courses);
    document.getElementById('myInput').value='';
}


function addRow(name, courses) {


    if (name !="") {
        // get the html table
        // 0 = the first table
        var table = document.getElementsByTagName('table')[0];

        // add new empty row to the table
        // 0 = in the top 
        // table.rows.length = the end
        // table.rows.length/2+1 = the center
        var newRow = table.insertRow(table.rows.length);

        // add cells to the row and values in cell
         newRow.insertCell(0).innerHTML=name;
         newRow.insertCell(1).innerHTML=courses[0];
         newRow.insertCell(2).innerHTML=courses[1];
         newRow.insertCell(3).innerHTML=courses[2];
    }
}
